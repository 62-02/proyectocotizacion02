/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import java.util.ArrayList;

/**
 *
 * @author felip
 */
public interface dbPersistencia {
    public void insertar(Object objeto) throws Exception;
    public void actualizar(Object objeto) throws Exception;
    public void borrar(Object objeto, String numCotizacion) throws Exception;
    public Object consultar(String codigo) throws Exception;
    
    public boolean isExists(String id) throws Exception;
    public ArrayList lista()throws Exception;
}
