/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import java.sql.*;
/**
 *
 * @author felip
 */
public class dbManejador {
    protected Connection conexion;
    protected PreparedStatement sqlConsulta;
    protected ResultSet registros;
    private String user;
    private String database;
    private String password;
    private String drive;
    private String url;

    public dbManejador(Connection conexion, PreparedStatement sqlConsulta, ResultSet registros, String user, String database, String password, String drive, String url) {
        this.conexion = conexion;
        this.sqlConsulta = sqlConsulta;
        this.registros = registros;
        this.user = user;
        this.database = database;
        this.password = password;
        this.drive = drive;
        this.url = url;
        EsDrive();
    }
    
    public dbManejador() {
        this.drive = "com.mysql.cj.jdbc.Driver";
        this.user = "admin";
        this.password = "ramon2008";
        this.url = "jdbc:mysql://microinformaticamx.cmwe0ss4z5wt.us-east-2.rds.amazonaws.com/sistema";
        EsDrive();
    }
    
    //AQUI ESTA NUESTRO dbManejador antiguo
   /* public dbManejador() {
        this.drive = "com.mysql.jdbc.Driver";
        this.database = "sistemas";
        this.user = "root";
        this.password = "";
        this.url = "jdbc:mysql://localhost/sistemas";
        EsDrive();
    }*/
    
    public boolean EsDrive(){
        boolean exito = false;
        try{
           Class.forName(drive);
           exito = true;
        }catch(ClassNotFoundException e){
            System.err.println("Surgio un error" + e.getMessage());
            System.exit(-1);
        }
        return exito;
    }
    
    public PreparedStatement getSqlConsulta() {
        return sqlConsulta;
    }

    public void setSqlConsulta(PreparedStatement sqlConsulta) {
        this.sqlConsulta = sqlConsulta;
    }
    
    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public ResultSet getRegistros() {
        return registros;
    }

    public void setRegistros(ResultSet registros) {
        this.registros = registros;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDrive() {
        return drive;
    }

    public void setDrive(String drive) {
        this.drive = drive;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
     public boolean conectar(){
        boolean exito = false;
        try{
            this.setConexion((DriverManager.getConnection(this.url, this.user, this.password)));
            exito = true;
        
        }catch(SQLException e){
            System.err.println("Surgio un error al conectar: " + e.getMessage());
        }
        return exito;
    }
    
    public void desconectar(){
        try{
            if(!this.conexion.isClosed()) this.getConexion().isClosed();
        }catch(SQLException e){
            System.err.println("No fue posible cerrar la sesion: " + e.getMessage());
            
        }
    }
    
}