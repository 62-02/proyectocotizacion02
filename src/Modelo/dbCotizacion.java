/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author feli`    !p
 */
public class dbCotizacion extends dbManejador implements dbPersistencia{
    
    @Override
    public void insertar(Object objeto) throws Exception {
        Cotizacion cot = new Cotizacion();
        cot = (Cotizacion) objeto;
        String consulta = "";
        
        consulta = "INSERT INTO "
                    + "cotizaciones(numCotizacion, descripcion, precio, porcentaje, plazos)"
                    + " VALUES (?,?,?,?,?)";
        
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
               
                //ASIGNAR VALORES
                this.sqlConsulta.setString(1, cot.getNumCotizacion());
                this.sqlConsulta.setString(2, cot.getDescripcionAutomibil());
                this.sqlConsulta.setFloat(3, cot.getPrecioAutomovil());
                this.sqlConsulta.setInt(4, cot.getPorcentajePago());
                this.sqlConsulta.setInt(5, cot.getPlazo());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }catch(SQLException e){
                System.err.println("Error al insertar " + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse");
        }
    }

    @Override
    public void actualizar(Object objeto) throws Exception {
        Cotizacion cot = new Cotizacion();
        cot = (Cotizacion) objeto;
        String consulta = "";
        
        consulta = "UPDATE cotizaciones set descripcion = ?, precio = ?, "
                    + "porcentaje = ?, plazos = ? WHERE numCotizacion = ?";
        
        if(conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                
                //ACTUALIZAR VALORES
                this.sqlConsulta.setString(1, cot.getDescripcionAutomibil());
                this.sqlConsulta.setFloat(2, cot.getPrecioAutomovil());
                this.sqlConsulta.setInt(3, cot.getPorcentajePago());
                this.sqlConsulta.setInt(4, cot.getPlazo());
                this.sqlConsulta.setString(5, cot.getNumCotizacion());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }catch(SQLException e){
                System.err.println("Error al actualizar " + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse");
        }
    }

    @Override
    public void borrar(Object objeto, String numCotizacion) throws Exception {
        Cotizacion cot = new Cotizacion();
        cot = (Cotizacion) objeto;
        String consulta = "DELETE from cotizaciones WHERE numCotizacion = ?";
                
        if(conectar()){
            try{
                System.err.println("Se conecto");
                 this.sqlConsulta = this.conexion.prepareStatement(consulta);
                

                this.sqlConsulta.setString(1, numCotizacion);
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }catch(SQLException e){
                System.out.println("Error al borrar " + e.getMessage());
            }
        }else{
            System.out.println("No fue posible conectarse");
        }
    }

    @Override
    public Object consultar(String codigo) throws Exception {
        Cotizacion cot = new Cotizacion();
        if(conectar()){
            String consulta = "SELECT * from cotizaciones WHERE numCotizacion = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);

            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                cot.setIdCotizacion(this.registros.getInt("numCotizacion"));
                cot.setDescripcionAutomovil(this.registros.getString("descripcion"));
                cot.setPrecioAutomovil(this.registros.getInt("precio"));
                cot.setPorcentajePago(this.registros.getInt("porcentaje"));
                cot.setPlazo(this.registros.getInt("plazos"));
            }
        }
        this.desconectar();
        return cot;
    }
    
    @Override
    public boolean isExists(String id) throws Exception {
        if(this.conectar()) {
            String consulta = "SELECT * from cotizaciones WHERE numCotizacion = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            
            // Asignar valores
            this.sqlConsulta.setString(1, id);
            
            // Hacer la consulta
            this.registros = this.sqlConsulta.executeQuery();
            
            // Se checa si el registro del código existe
            if(this.registros.next()) {
                if (this.registros.getString("numCotizacion") != null)
                    return true;
                else
                    return false;
            }
        }
        this.desconectar();
        return false;
    }

    @Override
    public ArrayList lista() throws Exception {
        ArrayList<Cotizacion> lista = new ArrayList<Cotizacion>();
        Cotizacion cot;
        
        if(this.conectar()){
            String consulta = "SELECT * from cotizaciones ORDER BY numCotizacion";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            
            while(this.registros.next()){
                cot = new Cotizacion();
                
                cot.setIdCotizacion(this.registros.getInt("idCotizacion"));
                cot.setNumCotizacion(this.registros.getString("numCotizacion"));
                cot.setDescripcionAutomovil(this.registros.getString("descripcion"));
                cot.setPrecioAutomovil(this.registros.getFloat("precio"));
                cot.setPorcentajePago(this.registros.getInt("porcentaje"));
                cot.setPlazo(this.registros.getInt("plazos"));
                lista.add(cot);
            }
        }
        this.desconectar();
        return lista;
    }
    
}