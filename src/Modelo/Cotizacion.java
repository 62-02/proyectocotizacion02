
package Modelo;

/**
 *
 * @author felip
 */
public class Cotizacion {
    private int idCotizacion;
    private String numCotizacion;
    private String descripcionAutomovil;
    private float precioAutomovil;
    private int porcentajePago;
    private int plazo;

    //METODOS CONSTRUCTORES
    public Cotizacion(){
    this.idCotizacion =0;
    this.numCotizacion = "";
    this.descripcionAutomovil = " ";
    this.precioAutomovil = 0;
    this.porcentajePago = 0;
    this.plazo = 0;
    }

    public Cotizacion(String numCotizacion, String descripcionAutomovil, float precioAutomovil, int
   porcentajePago, int plazo){
    this.numCotizacion = numCotizacion;
    this.descripcionAutomovil = descripcionAutomovil;
    this.precioAutomovil = precioAutomovil;
    this.porcentajePago = porcentajePago;
    this.plazo = plazo;
    }

    public Cotizacion(Cotizacion x){
    this.numCotizacion = x.numCotizacion;
    this.descripcionAutomovil = x.descripcionAutomovil;
    this.precioAutomovil = x.precioAutomovil;
    this.porcentajePago = x.porcentajePago;
    this.plazo = x.plazo;
    }
    
    //METODOS PARA CAMBIAR O CONOCER EL ESTADO DE LOS OBJETOS
    public void setIdCotizacion(int idCotizacion){
    this.idCotizacion = idCotizacion;
    }

    public void setNumCotizacion(String numCotizacion){
    this.numCotizacion = numCotizacion;
    }

    public void setDescripcionAutomovil(String descripcionAutomovil){
    this.descripcionAutomovil = descripcionAutomovil;
    }

    public void setPrecioAutomovil(float precioAutomovil){
    this.precioAutomovil = precioAutomovil;
    }

    public void setPorcentajePago( int porcentajePago){
    this.porcentajePago = porcentajePago;
    }

    public void setPlazo(int plazo){
    this.plazo = plazo;
    }

    public String getNumCotizacion(){
    return this.numCotizacion;
    }

    public int getIdCotizacion() {
    return idCotizacion;
    }

    public String getDescripcionAutomibil(){
    return this.descripcionAutomovil;
    }

    public float getPrecioAutomovil(){
    return this.precioAutomovil;
    }

    public int getPorcentajePago(){
    return this.porcentajePago;
    }

    public int getPlazo(){
    return this.plazo;
    }

    //METODOS DE COMPORTAMIENTO
    public float calcularPagoInicial(){
    return this.precioAutomovil * this.porcentajePago/ 100;
    }

    public float calcularTotalAfinanciar(){
    return this.precioAutomovil - this.calcularPagoInicial();
    }

    public float calcularPagoMensual(){
    return this.calcularTotalAfinanciar() / this.plazo;
    }
}