package Controlador;

import Vista.dlgCotizacion;
import Modelo.Cotizacion;
import Modelo.dbCotizacion;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class Controlador implements ActionListener {

    private dlgCotizacion vista;
    private Cotizacion C;
    private dbCotizacion CD;
    private boolean A = false;

    public Controlador(Cotizacion C, dbCotizacion CD, dlgCotizacion vista) {
        this.C = C;
        this.vista = vista;
        this.CD = CD;
        vista.btnCerrar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnBorrar.addActionListener(this);
    }

    private void iniciarVista() {
        vista.setTitle(":: Productos ::");
        vista.setSize(600, 620);
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }

    private void limpiar() {
        vista.lblPagoInicial.setText("");
        vista.lblPagoMensual.setText("");
        vista.lblTotal.setText("");
        vista.spnPorcentaje.setValue(0);
        vista.txtDescripcion.setText("");
        vista.txtCodigo.setText("");
        vista.txtPrecio.setText("");
        vista.rbtn12M.setSelected(false);
        vista.rbtn24M.setSelected(false);
        vista.rbtn36M.setSelected(false);
        vista.rbtn48M.setSelected(false);
        A = (false);
    }

    private void Guardar() {
        C.setNumCotizacion(vista.txtCodigo.getText());
        C.setDescripcionAutomovil(vista.txtDescripcion.getText());
        C.setPrecioAutomovil(Float.parseFloat(vista.txtPrecio.getText()));
        C.setPorcentajePago((int) vista.spnPorcentaje.getValue());
        C.setIdCotizacion(Integer.parseInt(vista.txtCodigo.getText()));
        if (vista.rbtn12M.isSelected()) {
            C.setPlazo(12);
        }
        if (vista.rbtn24M.isSelected()) {
            C.setPlazo(24);
        }
        if (vista.rbtn36M.isSelected()) {
            C.setPlazo(36);
        }
        if (vista.rbtn48M.isSelected()) {
            C.setPlazo(48);
        }
    }

    private void deshabilitar() {
        vista.txtCodigo.setEnabled(false);
        vista.txtDescripcion.setEnabled(false);
        vista.txtPrecio.setEnabled(false);
        vista.rbtn12M.setEnabled(false);
        vista.rbtn24M.setEnabled(false);
        vista.rbtn36M.setEnabled(false);
        vista.rbtn48M.setEnabled(false);
        vista.spnPorcentaje.setEnabled(false);
        vista.lblPagoInicial.setEnabled(false);
        vista.lblPagoMensual.setEnabled(false);
        vista.lblTotal.setEnabled(false);

        vista.btnGuardar.setEnabled(false);
        vista.btnBuscar.setEnabled(false);
        vista.btnCancelar.setEnabled(false);
        vista.btnLimpiar.setEnabled(false);
        vista.btnBorrar.setEnabled(false);
    }

    private void Funciones() {
        vista.lblPagoInicial.setText(Float.toString(C.calcularPagoInicial()));
        vista.lblPagoMensual.setText(Float.toString(C.calcularPagoMensual()));
        vista.lblTotal.setText(Float.toString(C.calcularTotalAfinanciar()));
    }

    private void habilitar() {
        vista.txtCodigo.setEnabled(true);
        vista.txtDescripcion.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.rbtn12M.setEnabled(true);
        vista.rbtn24M.setEnabled(true);
        vista.rbtn36M.setEnabled(true);
        vista.rbtn48M.setEnabled(true);
        vista.spnPorcentaje.setEnabled(true);
        vista.lblPagoInicial.setEnabled(true);
        vista.lblPagoMensual.setEnabled(true);
        vista.lblTotal.setEnabled(true);

        vista.btnGuardar.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnCancelar.setEnabled(true);
        vista.btnLimpiar.setEnabled(true);
        vista.btnBorrar.setEnabled(true);
    }

    private void cargarDatos() {
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Cotizacion> lista = new ArrayList<>();
        try {
            lista = CD.lista();
        } catch (Exception ex) {
        }
        modelo.addColumn("Id");
        modelo.addColumn("numCotizacion");
        modelo.addColumn("Descripcion");
        modelo.addColumn("Precio");
        modelo.addColumn("Porcentaje");
        modelo.addColumn("Plazo");
        for (Cotizacion producto : lista) {
            modelo.addRow(new Object[]{producto.getIdCotizacion(), producto.getNumCotizacion(), producto.getDescripcionAutomibil(), producto.getPrecioAutomovil(), producto.getPorcentajePago(), producto.getPlazo()});
        }
        vista.tableDatos.setModel(modelo);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnNuevo) {
            cargarDatos();
            limpiar();
            habilitar();
        }

        if (e.getSource() == vista.btnGuardar) {
            try {
                C.setNumCotizacion(vista.txtCodigo.getText());
                C.setDescripcionAutomovil(vista.txtDescripcion.getText());
                C.setPrecioAutomovil(Float.parseFloat(vista.txtPrecio.getText()));
                C.setPorcentajePago((int) vista.spnPorcentaje.getValue());
                C.setIdCotizacion(Integer.parseInt(vista.txtCodigo.getText()));
                if (vista.rbtn12M.isSelected()) {
                    C.setPlazo(12);
                }
                if (vista.rbtn24M.isSelected()) {
                    C.setPlazo(24);
                }
                if (vista.rbtn36M.isSelected()) {
                    C.setPlazo(36);
                }
                if (vista.rbtn48M.isSelected()) {
                    C.setPlazo(48);
                }
                if (!CD.isExists(vista.txtCodigo.getText())) {
                    A = true;
                    if (A) {
                        CD.insertar(C);
                        JOptionPane.showMessageDialog(null, "Registro Guardado");
                        Funciones();
                        limpiar();
                        cargarDatos();
                    }
                } else {
                    CD.actualizar(C);
                    JOptionPane.showMessageDialog(null, "Registro Actulizado");
                    Funciones();
                    limpiar();
                    cargarDatos();
                }
            } catch (SQLException d) {
                d.printStackTrace();
            } catch (Exception ex1) {
                System.err.println("Surgió un error" + ex1);
            }
        }
        if (e.getSource() == vista.btnBuscar) {
            try {
                Cotizacion res = new Cotizacion();
                if (CD.isExists(vista.txtCodigo.getText())) {
                    res = (Cotizacion) CD.consultar(vista.txtCodigo.getText());
                    vista.txtCodigo.setEnabled(false);
                    vista.txtDescripcion.setText(res.getDescripcionAutomibil());
                    vista.txtPrecio.setText(String.valueOf(res.getPrecioAutomovil()));
                    vista.spnPorcentaje.setValue(res.getPorcentajePago());

                    if (res.getPlazo() == 12) {
                        vista.rbtn12M.setSelected(true);
                    }
                    if (res.getPlazo() == 24) {
                        vista.rbtn24M.setSelected(true);
                    }
                    if (res.getPlazo() == 36) {
                        vista.rbtn36M.setSelected(true);
                    }
                    if (res.getPlazo() == 48) {
                        vista.rbtn48M.setSelected(true);
                    }
                    this.A = true;
                    JOptionPane.showMessageDialog(vista, "Se localizo");
                    Guardar();
                    Funciones();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(vista, "codigo no existe");
                System.out.println("Surgió un error" + ex.getMessage());
                vista.btnLimpiar.doClick();
            }
        }
        
        if (e.getSource() == vista.btnBorrar) {
                    try {
                        CD.borrar(C, vista.txtCodigo.getText());
                        cargarDatos();
                        JOptionPane.showMessageDialog(vista, "Se borrò exitosamente");
                        limpiar();
                    } catch (Exception ex) {
                        System.out.println("Surgió un error" + ex.getMessage());
                    }
             }

        if (e.getSource() == vista.btnCerrar) {
            int option = JOptionPane.showConfirmDialog(vista, "Seguro que quieres salir",
                    "Decide", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }

        if (e.getSource() == vista.btnCancelar) {
            limpiar();
            deshabilitar();
        }

        if (e.getSource() == vista.btnLimpiar) {
            limpiar();
        }
    }

    public static void main(String[] args) {
        Cotizacion C = new Cotizacion();
        dbCotizacion CD = new dbCotizacion();
        dlgCotizacion vista = new dlgCotizacion(new JFrame(), true);
        Controlador contra = new Controlador(C, CD, vista);
        contra.cargarDatos();
        contra.iniciarVista();
    }
}